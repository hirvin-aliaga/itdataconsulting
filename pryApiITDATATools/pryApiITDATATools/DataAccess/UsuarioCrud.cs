﻿using Microsoft.EntityFrameworkCore;
using pryApiITDATATools.Interfaces;
using pryApiITDATATools.Models;
using pryApiITDATATools.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace pryApiITDATATools.DataAccess
{
    public class UsuarioCrud : IUsuarioCrud
    {
        private readonly DB_ITDATA_SYSContext _context;

        public UsuarioCrud(DB_ITDATA_SYSContext context)
        {
            _context = context;
        }

        public Task<int> ActualizarUsuario(Usuario actualizaUsuario)
        {
            var _usuarioEncontrado = _context.Usuario.SingleOrDefault(b => b.Usuario1 == actualizaUsuario.Usuario1);

            _usuarioEncontrado.ApellidoMaterno = actualizaUsuario.ApellidoMaterno;
            _usuarioEncontrado.ApellidoPaterno = actualizaUsuario.ApellidoPaterno;
            _usuarioEncontrado.Contrasena = actualizaUsuario.Contrasena;
            _usuarioEncontrado.Correo = actualizaUsuario.Correo;
            _usuarioEncontrado.Dni = actualizaUsuario.Dni;
            _usuarioEncontrado.ImagenUsuario = actualizaUsuario.ImagenUsuario;
            _usuarioEncontrado.Nombres = actualizaUsuario.Nombres;
            _usuarioEncontrado.Usuario1 = actualizaUsuario.Usuario1;
            _usuarioEncontrado.Estado = actualizaUsuario.Estado;
            _usuarioEncontrado.UsuarioPerfil = actualizaUsuario.UsuarioPerfil;

            //_context.Usuario.Add(actualizaUsuario);
            _context.UsuarioPerfil.RemoveRange(_context.UsuarioPerfil.Where(x => x.IdUsuario== _usuarioEncontrado.IdUsuario));
            _context.SaveChanges();

            _context.Usuario.Update(_usuarioEncontrado);

            var _resultado = _context.SaveChangesAsync();

            return _resultado;
        }

        public async Task<Usuario> InsertarUsuarioAsync(Usuario nuevoUsuario)
        {
            await _context.Usuario.AddAsync(nuevoUsuario);

            var valor = await _context.SaveChangesAsync();
            if (valor==0)
            {
                Usuario vacio = new Usuario();
                return vacio;   
            }

            var _usuario = await _context.Usuario.FirstOrDefaultAsync(x => x.Usuario1.Equals(nuevoUsuario.Usuario1));


            return _usuario;
        }

        public async Task<int> InsertaUsuarioPerfilAsync(List<UsuarioPerfil> lstUsuarioPerfil)
        {
            int _commit = 0;
            foreach (var item in lstUsuarioPerfil)
            {
                await _context.UsuarioPerfil.AddAsync(item);

                var valor = await _context.SaveChangesAsync();
                _commit++;
            }

            return _commit;
        }

        public int ObtenerUsuario(string dato)
        {
            var _usuario =  _context.Usuario.FirstOrDefault(x => x.Usuario1.Equals(dato));


            return _usuario.IdUsuario;
        }

        public Usuario ObtenerUsuarioNombre(string nombreUsuario)
        {
            var _usuario = _context.Usuario.FirstOrDefault(x => x.Usuario1.Equals(nombreUsuario));



            return _usuario;
        }

        public List<Usuario> ObtenerUsuarios()
        {
            return  _context.Usuario.ToList();
        }

        public async Task<Usuario> ValidarUsuario(string usuario, string clave)
        {


            //var User;
            var User_usuario = await (from d in _context.Usuario
                              where d.Usuario1 == usuario.Trim() && d.Contrasena == clave.Trim()
                              select d).FirstOrDefaultAsync();

            return User_usuario;
        }

    }
}
