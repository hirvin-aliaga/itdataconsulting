﻿using System;
using System.Collections.Generic;

namespace pryWebAppItData.Models
{
    public partial class Usuario
    {
        public Usuario()
        {
            UsuarioPerfil = new HashSet<UsuarioPerfil>();
        }

        public int IdUsuario { get; set; }
        public string Nombres { get; set; }
        public string ApellidoPaterno { get; set; }
        public string ApellidoMaterno { get; set; }
        public string Correo { get; set; }
        public string Dni { get; set; }
        public string Usuario1 { get; set; }
        public string Contraseña { get; set; }
        public int Intentos { get; set; }
        public bool Bloqueado { get; set; }
        public string ImagenUsuario { get; set; }
        public string ImagenUsu { get; set; }

        public virtual ICollection<UsuarioPerfil> UsuarioPerfil { get; set; }
    }
}
