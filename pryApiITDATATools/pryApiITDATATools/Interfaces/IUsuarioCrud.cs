﻿using pryApiITDATATools.Models;
using pryApiITDATATools.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace pryApiITDATATools.Interfaces
{
    public interface IUsuarioCrud
    {
        Task<Usuario> ValidarUsuario(string usuario, string clave);
        List<Usuario> ObtenerUsuarios();
        Usuario ObtenerUsuarioNombre(string nombreUsuario);

        int ObtenerUsuario(string dato);
        Task<Usuario> InsertarUsuarioAsync(Usuario nuevoUsuario);
        Task<int> ActualizarUsuario(Usuario actualizaUsuario);

        Task<int> InsertaUsuarioPerfilAsync(List<UsuarioPerfil> lstUsuarioPerfil);

        


    }
}
