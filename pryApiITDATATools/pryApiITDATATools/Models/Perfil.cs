﻿using System;
using System.Collections.Generic;

namespace pryApiITDATATools.Models
{
    public partial class Perfil
    {
        public Perfil()
        {
            UsuarioPerfil = new HashSet<UsuarioPerfil>();
        }

        public int IdPerfil { get; set; }
        public string NombrePerfil { get; set; }

        public virtual ICollection<UsuarioPerfil> UsuarioPerfil { get; set; }
    }
}
