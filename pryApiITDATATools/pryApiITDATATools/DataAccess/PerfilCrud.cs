﻿using pryApiITDATATools.Interfaces;
using pryApiITDATATools.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace pryApiITDATATools.DataAccess
{
    public class PerfilCrud : IPerfilCrud
    {
        private readonly DB_ITDATA_SYSContext _context;
        public PerfilCrud(DB_ITDATA_SYSContext context)
        {
            _context = context;
        }

        public List<Perfil> ObtenerPerfles()
        {
            var _lstPerfiles = (from f in _context.Perfil
                              orderby f.NombrePerfil descending
                              select f).ToList();

            return _lstPerfiles;
        }

        public List<Perfil> ObtenerPerflesId(int idPerfil)
        {
            throw new NotImplementedException();
        }

        public Perfil ObtenerPerflesNombre(string nombrePerfil)
        {

            var _lstPerfil  = (from f in _context.Perfil
                             where f.NombrePerfil.Equals(nombrePerfil)
                             orderby f.NombrePerfil descending
                             select f).FirstOrDefault();

            return _lstPerfil;
        }
    }
}
