﻿using Microsoft.EntityFrameworkCore;
using pryWebAppItData.Interface;
using pryWebAppItData.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace pryWebAppItData.DataAccess
{
    public class UsuarioCrud : IUsuarioCrud
    {
        private readonly DB_ITDATA_SYSContext _context;

        public UsuarioCrud(DB_ITDATA_SYSContext context)
        {
            _context = context;
        }

        public async Task<List<Usuario>> ObtenerUsuarios()
        {
            return await _context.Usuario.ToListAsync();
        }

        public async Task<Usuario>  ValidarUsuario(string usuario, string clave)
        {          

     

             var User = (from d in _context.Usuario
                         where d.Usuario1 == usuario.Trim() && d.Contraseña == clave.Trim()
                         select d).FirstOrDefaultAsync();




            return await User;
        }

    }
}
