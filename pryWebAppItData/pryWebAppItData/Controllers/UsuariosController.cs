﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using pryWebAppItData.Interface;
using pryWebAppItData.Models;

namespace pryWebAppItData.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UsuariosController : ControllerBase
    {
        //private readonly DB_ITDATA_SYSContext _context;

        //public UsuariosController(DB_ITDATA_SYSContext context)
        //{
        //    _context = context;
        //}

        private readonly IUsuarioCrud _usuarioCrud;

        public UsuariosController(IUsuarioCrud usuarioCrud)
        {
            _usuarioCrud = usuarioCrud;
        }


        // GET: api/Usuarios
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Usuario>>> GetListaUsuarios()
        {
            var _listaUsuarios = await _usuarioCrud.ObtenerUsuarios();

            return _listaUsuarios;
        }


        // GET: api/Usuarios/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Usuario>> GetValidarUsuario(string user, string pwd)
        {
            var _validado = await _usuarioCrud.ValidarUsuario(user,pwd);
            //var usuario = await _context.Usuario.FindAsync(id);

            if (_validado == null)
            {
                return NotFound();
            }

            return _validado;
        }

        //// PUT: api/Usuarios/5
        //[HttpPut("{id}")]
        //public async Task<IActionResult> PutUsuario(int id, Usuario usuario)
        //{
        //    if (id != usuario.IdUsuario)
        //    {
        //        return BadRequest();
        //    }

        //    _context.Entry(usuario).State = EntityState.Modified;

        //    try
        //    {
        //        await _context.SaveChangesAsync();
        //    }
        //    catch (DbUpdateConcurrencyException)
        //    {
        //        if (!UsuarioExists(id))
        //        {
        //            return NotFound();
        //        }
        //        else
        //        {
        //            throw;
        //        }
        //    }

        //    return NoContent();
        //}

        //// POST: api/Usuarios
        //[HttpPost]
        //public async Task<ActionResult<Usuario>> PostUsuario(Usuario usuario)
        //{
        //    _context.Usuario.Add(usuario);
        //    await _context.SaveChangesAsync();

        //    return CreatedAtAction("GetUsuario", new { id = usuario.IdUsuario }, usuario);
        //}

        //// DELETE: api/Usuarios/5
        //[HttpDelete("{id}")]
        //public async Task<ActionResult<Usuario>> DeleteUsuario(int id)
        //{
        //    var usuario = await _context.Usuario.FindAsync(id);
        //    if (usuario == null)
        //    {
        //        return NotFound();
        //    }

        //    _context.Usuario.Remove(usuario);
        //    await _context.SaveChangesAsync();

        //    return usuario;
        //}

        //private bool UsuarioExists(int id)
        //{
        //    return _context.Usuario.Any(e => e.IdUsuario == id);
        //}




    }
}
