﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace pryApiITDATATools.ViewModels
{
    public class NuevoUsuario
    {
        public string IdUsuario { get; set; }
        public string Nombres { get; set; }
        public string ApellidoPaterno { get; set; }
        public string ApellidoMaterno { get; set; }
        public string Correo { get; set; }
        public string ImagenUsuario { get; set; }
        public string Dni { get; set; }
        public string Usuario1 { get; set; }
        public string Contrasena { get; set; }
        public List<PerfilModelo> NombrePerfil { get; set; }
        public string Estado { get; set; }


    }
}
