﻿using pryWebAppItData.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace pryWebAppItData.Interface
{
    public interface IUsuarioCrud
    {
        Task<Usuario> ValidarUsuario(string usuario, string clave);
        Task<List<Usuario>> ObtenerUsuarios();
    }
}
