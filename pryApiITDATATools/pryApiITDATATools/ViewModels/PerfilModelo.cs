﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace pryApiITDATATools.ViewModels
{
    public class PerfilModelo
    {
        public PerfilModelo(int idPerfil, string nombrePerfil)
        {
            IdPerfil = idPerfil;
            NombrePerfil = nombrePerfil;
        }
        public PerfilModelo()
        {
        }
        public int IdPerfil { get; set; }
        public string NombrePerfil { get; set; }
    }
}
