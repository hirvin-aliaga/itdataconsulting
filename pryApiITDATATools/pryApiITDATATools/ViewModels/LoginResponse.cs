﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace pryApiITDATATools.ViewModels
{
    public class LoginResponse
    {
        public string Token { get; set; }
        public string Nombres { get; set; }
        public string ApellidoPaterno { get; set; }
        public string ApellidoMaterno { get; set; }
        public string ImagenUsuario { get; set; }
        public List<String> NombrePerfil { get; set; }

        
    }
}
