﻿using pryApiITDATATools.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace pryApiITDATATools.Interfaces
{
    public interface IPerfilCrud
    {   
        Perfil ObtenerPerflesNombre(string nombrePerfil);
        List<Perfil> ObtenerPerflesId(int idPerfil);
        List<Perfil> ObtenerPerfles();


    }
}
