﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using pryApiITDATATools.Interfaces;
using pryApiITDATATools.Models;
using pryApiITDATATools.ViewModels;

namespace pryApiITDATATools.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [EnableCors("able")]
    public class UsuarioController : ControllerBase
    {
        private readonly IConfiguration _configuration;
        private readonly IUsuarioCrud _usuarioCrud;
        private readonly IPerfilCrud _perfilCrud;

        public UsuarioController(IUsuarioCrud usuarioCrud, IPerfilCrud perfilCrud,
            IConfiguration configuration)
        {
            _usuarioCrud = usuarioCrud;
            _perfilCrud = perfilCrud;
            this._configuration = configuration;
        }


        // GET: api/Usuarios
        [HttpGet]
        [Route("ListaUsuarios")]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public ActionResult<IEnumerable<NuevoUsuario>> GetListaUsuarios()
        {
            var _listaUsuarios = _usuarioCrud.ObtenerUsuarios();

            List<NuevoUsuario> _lstUsuarios = new List<NuevoUsuario>();
            foreach (var item in _listaUsuarios)
            {
                NuevoUsuario _modelo = new NuevoUsuario();
                _modelo.ApellidoMaterno = item.ApellidoMaterno;
                _modelo.ApellidoPaterno = item.ApellidoPaterno;
                _modelo.Correo = item.Correo;
                _modelo.Dni = item.Dni;
                _modelo.Estado = item.Estado;
                _modelo.ImagenUsuario = item.ImagenUsuario;
                _modelo.Nombres = item.Nombres;
                _modelo.Usuario1 = item.Usuario1;

                List<PerfilModelo> _lstModeloPerfil = new List<PerfilModelo>();

                foreach (var item2 in item.UsuarioPerfil)
                {
                    PerfilModelo _modeloPerfil = new PerfilModelo();
                    _modeloPerfil.NombrePerfil = item2.IdPerfilNavigation.NombrePerfil;
                    _modeloPerfil.IdPerfil = item2.IdPerfilNavigation.IdPerfil;

                    _lstModeloPerfil.Add(_modeloPerfil);
                }

                _modelo.NombrePerfil = _lstModeloPerfil;

                _lstUsuarios.Add(_modelo);
            }


            return _lstUsuarios;
        }

        // GET: api/Usuarios/5
        //[HttpPost("{id}")]

        [HttpPost]
        [Route("Validar")]
        public async Task<ActionResult<LoginResponse>> GetValidarUsuario([FromBody] LoginRequest loginRequest)
        {
            if (ModelState.IsValid)
            {
                var _validado = await _usuarioCrud.ValidarUsuario(loginRequest.usuario, loginRequest.password);

                if (_validado == null)
                {
                    ModelState.AddModelError(string.Empty, "Invalid login attempt.");
                    return BadRequest(ModelState);
                    //return NotFound();
                }

                var _responseLogin = new LoginResponse();

                _responseLogin.Token = BuildToken(_validado);
                _responseLogin.ApellidoMaterno = _validado.ApellidoMaterno;
                _responseLogin.ApellidoPaterno = _validado.ApellidoPaterno;
                _responseLogin.ImagenUsuario = _validado.ImagenUsuario;
                _responseLogin.NombrePerfil = new List<string>();
                foreach (var item in _validado.UsuarioPerfil)
                {
                    _responseLogin.NombrePerfil.Add(item.IdPerfilNavigation.NombrePerfil.ToString());

                }

                _responseLogin.Nombres = _validado.Nombres;

                return _responseLogin;

            }
                else
            {
                return BadRequest(ModelState);
            }

        }

        [HttpPost]
        [Route("CrearUsuario")]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public async Task<ActionResult> CrearUsuario([FromBody] NuevoUsuario nuevoUsuario)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var _usuario = new Usuario();
            _usuario.ApellidoMaterno = nuevoUsuario.ApellidoMaterno;
            _usuario.ApellidoPaterno = nuevoUsuario.ApellidoPaterno;
            _usuario.Contrasena = nuevoUsuario.Contrasena;
            _usuario.Correo = nuevoUsuario.Correo;
            _usuario.Dni = nuevoUsuario.Dni;
            _usuario.ImagenUsuario = nuevoUsuario.ImagenUsuario;
            _usuario.Nombres = nuevoUsuario.Nombres;
            _usuario.Usuario1 = nuevoUsuario.Usuario1;
            _usuario.Estado = "Activo" ;
            

            var _usuarioInsertado = await _usuarioCrud.InsertarUsuarioAsync(_usuario);

            if (_usuarioInsertado == null)
            {
                return BadRequest();
            }
            
            List<UsuarioPerfil> _lstUsuarioPerfil = new List<UsuarioPerfil>();

            foreach (var item in nuevoUsuario.NombrePerfil)
            {
                Perfil _perfil = new Perfil();
                _perfil.IdPerfil=item.IdPerfil;
                _perfil.NombrePerfil = item.NombrePerfil;

                UsuarioPerfil _usuarioPerfil = new UsuarioPerfil();
                _usuarioPerfil.IdPerfil = _perfil.IdPerfil;
                _usuarioPerfil.IdUsuario = _usuarioInsertado.IdUsuario;

                _lstUsuarioPerfil.Add(_usuarioPerfil);
            }

            var _lstInsertada = await _usuarioCrud.InsertaUsuarioPerfilAsync(_lstUsuarioPerfil);

            return Ok();
        }

        [HttpPost]
        [Route("ActualizarUsuario")]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public async Task<ActionResult> ActualizarUsuario([FromBody] NuevoUsuario nuevoUsuario)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            var _idUsuario = _usuarioCrud.ObtenerUsuario(nuevoUsuario.Usuario1);

            var _usuario = new Usuario();
            _usuario.ApellidoMaterno = nuevoUsuario.ApellidoMaterno;
            _usuario.ApellidoPaterno = nuevoUsuario.ApellidoPaterno;
            _usuario.Contrasena = nuevoUsuario.Contrasena;
            _usuario.Correo = nuevoUsuario.Correo;
            _usuario.Dni = nuevoUsuario.Dni;
            _usuario.ImagenUsuario = nuevoUsuario.ImagenUsuario;
            _usuario.Nombres = nuevoUsuario.Nombres;
            _usuario.Usuario1 = nuevoUsuario.Usuario1;
            _usuario.Estado = nuevoUsuario.Estado;

            List<UsuarioPerfil> _lstUsuarioPerfil = new List<UsuarioPerfil>();

            foreach (var item in nuevoUsuario.NombrePerfil)
            {
                Perfil _perfil = new Perfil();
                _perfil.IdPerfil = item.IdPerfil;
                _perfil.NombrePerfil = item.NombrePerfil;

                UsuarioPerfil _usuarioPerfil = new UsuarioPerfil();
                _usuarioPerfil.IdPerfil = _perfil.IdPerfil;
                _usuarioPerfil.IdUsuario = _idUsuario;

                _lstUsuarioPerfil.Add(_usuarioPerfil);
            }
            _usuario.UsuarioPerfil = _lstUsuarioPerfil;

            var _usuarioActualiza = await _usuarioCrud.ActualizarUsuario(_usuario);

            if (_usuarioActualiza == 0)
            {
                return BadRequest();
            }

            //var _lstInsertada = await _usuarioCrud.InsertaUsuarioPerfilAsync(_lstUsuarioPerfil);

            return Ok();
        }





        [HttpGet]
        [Route("ObtenerPerfiles")]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public ActionResult<List<PerfilModelo>> ObtenerPerfiles()
        {
            var _lstPerfiles = _perfilCrud.ObtenerPerfles();
            List<PerfilModelo> _lstModeloPerfil = new List<PerfilModelo>();
            
            foreach (var item in _lstPerfiles)
            {
                PerfilModelo _modeloPerfil = new PerfilModelo();
                _modeloPerfil.IdPerfil = item.IdPerfil;
                _modeloPerfil.NombrePerfil = item.NombrePerfil;
                _lstModeloPerfil.Add(_modeloPerfil);
            }


            return _lstModeloPerfil;
        }


        [HttpGet]
        [Route("ObtenerUsuario")]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public ActionResult<NuevoUsuario> ObtenerUsuario(string nombreUsuario)
        {
            var _usuario = _usuarioCrud.ObtenerUsuarioNombre(nombreUsuario);
            NuevoUsuario _modelo = new NuevoUsuario();
            _modelo.ApellidoMaterno = _usuario.ApellidoMaterno;
            _modelo.ApellidoPaterno = _usuario.ApellidoPaterno;
            _modelo.Correo = _usuario.Correo;
            _modelo.Dni = _usuario.Dni;
            _modelo.Estado = _usuario.Estado;
            _modelo.ImagenUsuario = _usuario.ImagenUsuario;
            _modelo.Nombres= _usuario.Nombres;
            _modelo.Usuario1= _usuario.Usuario1;

            List<PerfilModelo> _lstModeloPerfil = new List<PerfilModelo>();

            foreach (var item in _usuario.UsuarioPerfil)
            {
                PerfilModelo _modeloPerfil = new PerfilModelo();
                _modeloPerfil.NombrePerfil = item.IdPerfilNavigation.NombrePerfil;
                _modeloPerfil.IdPerfil = item.IdPerfilNavigation.IdPerfil;

                _lstModeloPerfil.Add(_modeloPerfil);
            }

            _modelo.NombrePerfil = _lstModeloPerfil; 

            return _modelo;
        }




        private string BuildToken(Usuario validadoUsuario)
        {
            var claims = new[]
            {
                new Claim(JwtRegisteredClaimNames.UniqueName, validadoUsuario.IdUsuario.ToString()),
                new Claim(JwtRegisteredClaimNames.NameId, validadoUsuario.Usuario1), 
                new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString())
            };

            var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes("Xpp46bkyd4887vh24VHI"));
            var creds = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);

            var expiration = DateTime.UtcNow.AddHours(1);

            JwtSecurityToken token = new JwtSecurityToken(
               issuer: "yourdomain.com",
               audience: "yourdomain.com",
               claims: claims,
               expires: expiration,
               signingCredentials: creds);
            var dato = new JwtSecurityTokenHandler().WriteToken(token);

            return dato;

        }


    }
}